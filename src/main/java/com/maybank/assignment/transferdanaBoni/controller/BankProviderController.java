package com.maybank.assignment.transferdanaBoni.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;
import com.maybank.assignment.transferdanaBoni.service.BankProviderService;

@Controller
@RequestMapping("/bankprovider")
public class BankProviderController {
	@Autowired
	private BankProviderService bankProviderService;
	@GetMapping
	public String index (String keyword, Model model) {
		List<BankProvider> bankprovider = this.bankProviderService.getAll();
		model.addAttribute("bankProviderForm",new BankProvider());
		model.addAttribute("provider",bankprovider);
		return "bankprovider";
    }
	
	@PostMapping("/save")
    public String save(@Valid @ModelAttribute("bankproviderForm") BankProvider bankProvider,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            Model model) {
        System.out.println("Employee Name: "+bankProvider.getNamaBank());
        this.bankProviderService.save(bankProvider);
        redirectAttributes.addFlashAttribute("Data Berhasil diinput");
        return "redirect:/bankprovider";
    }

	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long Id) {
		Optional<BankProvider> bankprovider = this.bankProviderService.getBankProviderbyId(Id);
		if(bankprovider.isPresent()) {
		this.bankProviderService.delete(bankprovider.get());
	}
		return "redirect:/bankprovider";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam("id") Long Id, Model model) {
		Optional<BankProvider> bankprovider = this.bankProviderService.getBankProviderbyId(Id);		
		model.addAttribute("bankProviderForm", bankprovider);
		return "edit-bankprovider";
	}
	
}