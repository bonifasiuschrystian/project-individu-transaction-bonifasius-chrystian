package com.maybank.assignment.transferdanaBoni.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.assignment.transferdanaBoni.entity.Nasabah;
import com.maybank.assignment.transferdanaBoni.repository.NasabahRepository;


@Controller
@RequestMapping("/")

public class HomeController {
	
	@Autowired
	private NasabahRepository nasabahRepository;
	
	@GetMapping
	public String index (Model model) {
		List<Nasabah> nasabah = this.nasabahRepository.findAll();
		String springMessage = "hello view";
		model.addAttribute("nasabah",nasabah);
		model.addAttribute("SpringMessage",springMessage);
		return "home";
	}
	}

