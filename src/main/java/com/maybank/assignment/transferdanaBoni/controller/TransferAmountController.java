package com.maybank.assignment.transferdanaBoni.controller;

import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;
import com.maybank.assignment.transferdanaBoni.entity.Nasabah;
import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;
import com.maybank.assignment.transferdanaBoni.entity.TransferAmount;
import com.maybank.assignment.transferdanaBoni.service.BankProviderService;
import com.maybank.assignment.transferdanaBoni.service.NasabahService;
import com.maybank.assignment.transferdanaBoni.service.TambahRekeningService;
import com.maybank.assignment.transferdanaBoni.service.TransferAmountService;

@Controller
@RequestMapping("/transaksi")
public class TransferAmountController {
	@Autowired
	private TambahRekeningService tambahRekeningService;
	@Autowired
	private NasabahService nasabahService;
	@Autowired
	private BankProviderService bankProviderService;
	@Autowired
	private TransferAmountService transferAmountService;
	
	@GetMapping
	public String index(Model model) {
	List<BankProvider> provider = this.bankProviderService.getAll();
	List<TambahRekening> tambahrekening = this.tambahRekeningService.getAll();
	List<TransferAmount> transferamount = this.transferAmountService.getAll();
	
	model.addAttribute("addrekeningForm",new TambahRekening());
	model.addAttribute("nasabahForm",new Nasabah());
	model.addAttribute("bankproviderForm", new BankProvider());
	model.addAttribute("transferForm", new TransferAmount());
	
	model.addAttribute("tambahrekening", tambahrekening);
	model.addAttribute("provider",provider);
	model.addAttribute("transferamount",transferamount);
	
	return "transaksi";
	}
	
	@PostMapping("/save")
    public String save(
            @ModelAttribute("nasabahForm")Nasabah nasabah,
            @ModelAttribute("tambahrekeningForm")TambahRekening tambahRekening,
            @ModelAttribute("bankproviderForm")BankProvider bankProvider, 
            @ModelAttribute("transferamountForm") TransferAmount transferamount, Model model){
		
		List<TambahRekening> tambahrekening2 = this.tambahRekeningService.getAll();
		List<TransferAmount> transferamount2 = this.transferAmountService.getAll();
		TambahRekening rekpengirim = tambahRekeningService.findByrekening(transferamount.getRekpengirim().getRekening());
		TambahRekening rekpenerima = tambahRekeningService.findByrekening(transferamount.getRekpenerima().getRekening());
		Long idPengirim = rekpengirim.getId();
		Long idPenerima =  rekpenerima.getId();
		
		Optional<TambahRekening> pengirim = this.tambahRekeningService.getRekeningById(idPengirim);
		Optional<TambahRekening> penerima = this.tambahRekeningService.getRekeningById(idPenerima);
		
		TambahRekening rekPengirim = pengirim.get();
		TambahRekening rekPenerima = penerima.get();
		
//		double amount = transferamount.getJumlahAmount();
        transferamount.setTambahRekening(tambahRekening);

//        this.tambahRekeningService.save(tambahRekening);
//        this.nasabahService.save(nasabah);
//        this.bankProviderService.save(bankProvider);
        this.transferAmountService.save(transferamount);

        return "redirect:/transaksi";
        }

}
