package com.maybank.assignment.transferdanaBoni.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;
import com.maybank.assignment.transferdanaBoni.entity.Nasabah;
import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;
import com.maybank.assignment.transferdanaBoni.repository.BankProviderRepository;
import com.maybank.assignment.transferdanaBoni.repository.NasabahRepository;
import com.maybank.assignment.transferdanaBoni.repository.TambahRekeningRepository;
import com.maybank.assignment.transferdanaBoni.service.BankProviderService;
import com.maybank.assignment.transferdanaBoni.service.NasabahService;
import com.maybank.assignment.transferdanaBoni.service.TambahRekeningService;

@Controller
@RequestMapping("/tambahrekening")
public class TambahRekeningController {
	@Autowired
	private NasabahService nasabahService;
	@Autowired
	private TambahRekeningService tambahRekeningService;
	@Autowired
	private BankProviderService bankProviderService;
	@GetMapping
	public String index (String keyword, Model model) {
		List<BankProvider> providers = this.bankProviderService.getAll();
		List<TambahRekening> rekenings = this.tambahRekeningService.getAll();
		
		model.addAttribute("tambahrekeningForm",new TambahRekening());
		model.addAttribute("nasabahForm",new Nasabah());
		
		model.addAttribute("providers",providers);
		model.addAttribute("rekenings",rekenings);
		return "tambahrekening";
    }
	
	@PostMapping("/save")
    public String save(@ModelAttribute("nasabahForm") Nasabah nasabah,
    		@ModelAttribute("tambahrekeningForm") TambahRekening tambahRekening,
    		@ModelAttribute("providers") BankProvider bankProvider,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            Model model) {
//        System.out.println("Employee Name: "+ tambahRekening.getNamaLengkap());
        System.out.println("Nama Lengkap"+nasabah.getNamaLengkap());
        tambahRekening.setBankProvider(bankProvider);
        tambahRekening.setNasabah(nasabah);
        tambahRekening.setSaldo(1000000.0);
		this.nasabahService.save(nasabah);
		this.tambahRekeningService.save(tambahRekening);
		this.bankProviderService.save(bankProvider);
        redirectAttributes.addFlashAttribute("Data Berhasil diinput");
        return "redirect:/tambahrekening";
    }

}
