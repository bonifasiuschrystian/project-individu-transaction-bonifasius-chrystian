package com.maybank.assignment.transferdanaBoni.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;

public interface TambahRekeningRepository extends JpaRepository<TambahRekening, Long> {
	TambahRekening findByRekening(String rekening);
	Optional<TambahRekening> getRekeningById(Long id);
}
