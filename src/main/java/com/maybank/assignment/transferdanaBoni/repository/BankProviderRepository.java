package com.maybank.assignment.transferdanaBoni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;

public interface BankProviderRepository extends JpaRepository<BankProvider, Long>{

}
