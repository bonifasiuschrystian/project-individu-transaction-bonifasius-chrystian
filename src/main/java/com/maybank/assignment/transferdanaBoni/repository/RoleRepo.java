package com.maybank.assignment.transferdanaBoni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {

}
