package com.maybank.assignment.transferdanaBoni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.TransferAmount;

public interface TransferAmountRepository extends JpaRepository<TransferAmount, Long>{
	
}
