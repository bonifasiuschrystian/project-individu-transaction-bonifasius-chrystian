package com.maybank.assignment.transferdanaBoni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.Nasabah;

public interface NasabahRepository extends JpaRepository<Nasabah, Long> {

}
