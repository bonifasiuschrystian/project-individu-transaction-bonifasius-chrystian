package com.maybank.assignment.transferdanaBoni.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.assignment.transferdanaBoni.entity.User;

public interface UserRepo extends JpaRepository<User, Long> {
	User findByUsername(String username);
	

}
