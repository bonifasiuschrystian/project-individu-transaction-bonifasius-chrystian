package com.maybank.assignment.transferdanaBoni.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.assignment.transferdanaBoni.service.CustomerUserDetailService;

@Configuration
@EnableWebSecurity
public class MyAuth {
	@Autowired
	private CustomerUserDetailService customerUserDetailService;
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customerUserDetailService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}
	@Bean
	public SecurityFilterChain securityFChain(HttpSecurity httpSecurity) throws Exception{
		httpSecurity.authorizeRequests().antMatchers("/bankprovider").hasAnyAuthority("admin");
		httpSecurity.authorizeRequests().antMatchers("/tambahrekening").hasAnyAuthority("cs");
		httpSecurity.authorizeRequests().antMatchers("/transaksi").hasAnyAuthority("operator");
		//httpSecurity.authorizeRequests().antMatchers("/transaksi").hasAnyAuthority("operator");

		 httpSecurity.csrf().disable();
	        httpSecurity.authorizeRequests().antMatchers("/api/**")
	        .authenticated().and().httpBasic();
//	        httpSecurity.authorizeRequests().anyRequest().authenticated();
	        httpSecurity.authorizeRequests().and().formLogin();

	        return httpSecurity.build();
	    }
	
	
}
