package com.maybank.assignment.transferdanaBoni.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "rekeningbaru")
public class TambahRekening {
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	
//	@NotEmpty
//	@NotBlank
//	@NotNull
//	@Size(min=4, max =12)
	@Column(name="rekening",nullable=false, unique = true)
	private String rekening;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekpenerima", cascade = javax.persistence.CascadeType.ALL)
	private List<TransferAmount> rekeningpenerima;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekpengirim", cascade = javax.persistence.CascadeType.ALL)
	private List<TransferAmount> rekeningpengirim;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="provider_id",referencedColumnName = "id")
	private BankProvider bankProvider;
	
	private Double saldo = 1000000.0;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="nasabah_id",referencedColumnName = "id")
	private Nasabah nasabah;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Nasabah getNasabah() {
		return nasabah;
	}
	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}
	public BankProvider getBankProvider() {
		return bankProvider;
	}
	public void setBankProvider(BankProvider bankProvider) {
		this.bankProvider = bankProvider;
	}
	public List<TransferAmount> getRekeningpenerima() {
		return rekeningpenerima;
	}
	public void setRekeningpenerima(List<TransferAmount> rekeningpenerima) {
		this.rekeningpenerima = rekeningpenerima;
	}
	public List<TransferAmount> getRekeningpengirim() {
		return rekeningpengirim;
	}
	public void setRekeningpengirim(List<TransferAmount> rekeningpengirim) {
		this.rekeningpengirim = rekeningpengirim;
	}
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	
	
	

}
