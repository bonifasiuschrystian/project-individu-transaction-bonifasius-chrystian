package com.maybank.assignment.transferdanaBoni.entity;

import java.sql.Date;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transferamount")
public class TransferAmount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private TambahRekening rekpengirim;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private TambahRekening rekpenerima;
	
	private Date tanggalKirim;
	private Double jumlahAmount;
	private Double fee;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="rekening_id", referencedColumnName = "id")
	private TambahRekening tambahRekening;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TambahRekening getRekpengirim() {
		return rekpengirim;
	}

	public void setRekpengirim(TambahRekening rekpengirim) {
		this.rekpengirim = rekpengirim;
	}

	public TambahRekening getRekpenerima() {
		return rekpenerima;
	}

	public void setRekpenerima(TambahRekening rekpenerima) {
		this.rekpenerima = rekpenerima;
	}

	public Date getTanggalKirim() {
		return tanggalKirim;
	}

	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}

	public Double getJumlahAmount() {
		return jumlahAmount;
	}

	public void setJumlahAmount(Double jumlahAmount) {
		this.jumlahAmount = jumlahAmount;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public TambahRekening getTambahRekening() {
		return tambahRekening;
	}

	public void setTambahRekening(TambahRekening tambahRekening) {
		this.tambahRekening = tambahRekening;
	}}
	
	