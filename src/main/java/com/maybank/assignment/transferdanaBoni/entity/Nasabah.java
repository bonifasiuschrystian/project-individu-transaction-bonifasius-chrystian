package com.maybank.assignment.transferdanaBoni.entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

@Entity
@Table(name = "nasabah")
public class Nasabah {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
//	@NotNull
//	@NotBlank
//	@NotEmpty
	private String namaLengkap;
	private String tanggalLahir;
	private String nomorIdentitas;
	private String tipeIdentitas;
	private String email;
	private String noKontak;
	
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "nasabah")
	private List<TambahRekening> list_rekening;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaLengkap() {
		return namaLengkap;
	}

	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getNomorIdentitas() {
		return nomorIdentitas;
	}

	public void setNomorIdentitas(String nomorIdentitas) {
		this.nomorIdentitas = nomorIdentitas;
	}

	public String getTipeIdentitas() {
		return tipeIdentitas;
	}

	public void setTipeIdentitas(String tipeIdentitas) {
		this.tipeIdentitas = tipeIdentitas;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoKontak() {
		return noKontak;
	}

	public void setNoKontak(String noKontak) {
		this.noKontak = noKontak;
	}

}
