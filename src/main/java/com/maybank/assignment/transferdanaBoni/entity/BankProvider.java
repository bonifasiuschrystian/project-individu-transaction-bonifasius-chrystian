package com.maybank.assignment.transferdanaBoni.entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "provider")

public class BankProvider {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private int kodeBank;
//	@NotNull
//	@NotBlank
//	@NotEmpty
//	@Column(nullable=false,unique=true)
	private String namaBank;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankProvider")
	private List<TambahRekening> list_rekening;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getKodeBank() {
		return kodeBank;
	}

	public void setKodeBank(int kodeBank) {
		this.kodeBank = kodeBank;
	}

	public String getNamaBank() {
		return namaBank;
	}

	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}

	public List<TambahRekening> getList_rekening() {
		return list_rekening;
	}

	public void setList_rekening(List<TambahRekening> list_rekening) {
		this.list_rekening = list_rekening;
	}
	
	

}
