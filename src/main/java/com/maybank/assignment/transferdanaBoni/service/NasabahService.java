package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.Nasabah;

@Service
public interface NasabahService {
	public List<Nasabah> getAll();
	public void save(Nasabah nasabah);
	

}
