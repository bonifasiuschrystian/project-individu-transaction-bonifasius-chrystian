package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;
import com.maybank.assignment.transferdanaBoni.repository.BankProviderRepository;


@Service
public class BankServiceImplementation implements BankProviderService{
	
	@Autowired
	public BankProviderRepository bankProviderRepository;

	@Override
	public List<BankProvider> getAll() {
		// TODO Auto-generated method stub
		return this.bankProviderRepository.findAll();
	}

	@Override
	public void save(BankProvider bankProvider) {
		// TODO Auto-generated method stub
		this.bankProviderRepository.save(bankProvider);
	}

	@Override
	public void delete(BankProvider bankProvider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<BankProvider> getBankProviderbyId(Long Id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}


}
