package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.Nasabah;
import com.maybank.assignment.transferdanaBoni.repository.NasabahRepository;

@Service
//@Transactional
public class NasabahServiceImplementation implements NasabahService{
	@Autowired
	private NasabahRepository nasabahRepository;

	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepository.findAll();
	}

	@Override
	public void save(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.nasabahRepository.save(nasabah);
	}

}
