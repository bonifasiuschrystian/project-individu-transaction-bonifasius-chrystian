package com.maybank.assignment.transferdanaBoni.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.Role;
import com.maybank.assignment.transferdanaBoni.entity.User;
import com.maybank.assignment.transferdanaBoni.repository.RoleRepo;
import com.maybank.assignment.transferdanaBoni.repository.UserRepo;


@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private UserRepo userRepo;
	
	@PostConstruct
	public void index() {
//		Create Role
		Role roleCs = new Role();
		Role roleAdmin = new Role();
		Role roleOperator = new Role();
		
		roleCs.setRole("cs");
		roleAdmin.setRole("admin");
		roleOperator.setRole("operator");
		this.roleRepo.save(roleCs);
		this.roleRepo.save(roleAdmin);
		this.roleRepo.save(roleOperator);
		
//		create user
		List<Role> listRole = new ArrayList<>();
		List<Role> userListRole = new ArrayList<>();
		List<Role> operatorListRole = new ArrayList<>();
		listRole.add(roleAdmin);
		listRole.add(roleCs);
		userListRole.add(roleCs);
		operatorListRole.add(roleOperator);
		
		User userAdmin = new User();
		userAdmin.setUsername("admink");
		userAdmin.setEmail("admink@maybank.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userAdmin.setRoles(listRole);
		
		User userCs = new User();
		userCs.setUsername("customerservice");
		userCs.setEmail("cs@maybank.com");
		userCs.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userCs.setRoles(userListRole);
		
		User userOperator = new User();
		userOperator.setUsername("operator");
		userOperator.setEmail("operator@maybank.com");
		userOperator.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userOperator.setRoles(operatorListRole);
		
//		this.userRepo.save(userAdmin);
//		this.userRepo.save(userCs);
//		this.userRepo.save(userOperator);
	}

}
