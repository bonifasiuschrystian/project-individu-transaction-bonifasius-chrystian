package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;
import java.util.Optional;

import com.maybank.assignment.transferdanaBoni.entity.TransferAmount;


public interface TransferAmountService {

	public List<TransferAmount> getAll();
		// TODO Auto-generated method stub
	public void save(TransferAmount transferamount);
	public void delete(Long Id);
	Optional<TransferAmount> getTfbyId (Long Id);

	}
//	public List<TransferAmount> getAmounts;
//

