package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.BankProvider;

@Service
public interface BankProviderService {
	public List<BankProvider> getAll();
	public void save(BankProvider bankProvider);
	public void delete(BankProvider bankProvider);
	public Optional<BankProvider> getBankProviderbyId(Long Id);
}
