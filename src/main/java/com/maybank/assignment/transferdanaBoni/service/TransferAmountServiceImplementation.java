package com.maybank.assignment.transferdanaBoni.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;
import com.maybank.assignment.transferdanaBoni.entity.TransferAmount;
import com.maybank.assignment.transferdanaBoni.repository.TambahRekeningRepository;
import com.maybank.assignment.transferdanaBoni.repository.TransferAmountRepository;

@Service
public class TransferAmountServiceImplementation implements TransferAmountService {

	@Autowired
	private TransferAmountRepository transferAmountRepository;
	
	@Autowired
	private TambahRekeningRepository tambahRekeningRepository;
	
	@Autowired TambahRekeningService tambahRekeningService;

	@Override
	public List<TransferAmount> getAll() {
		// TODO Auto-generated method stub
		return this.transferAmountRepository.findAll();
	}

	@Override
	public void save(TransferAmount transferamount) {
		// TODO Auto-generated method stub
		TambahRekening rekeningPengirim = tambahRekeningService.findByrekening(transferamount.getRekpengirim().getRekening());
		TambahRekening rekeningPenerima = tambahRekeningService.findByrekening(transferamount.getRekpenerima().getRekening());
		
		Long idPengirim = rekeningPengirim.getId();
		Long idPenerima = rekeningPenerima.getId();
		
		Optional<TambahRekening> pengirim = this.tambahRekeningService.getRekeningById(idPengirim);
		Optional<TambahRekening> penerima = this.tambahRekeningService.getRekeningById(idPenerima);
		
		TambahRekening rekeningpengirim = pengirim.get();
		TambahRekening rekeningpenerima = penerima.get();
		
		String asalBank = rekeningpengirim.getBankProvider().getNamaBank();
		String tujuanBank = rekeningpenerima.getBankProvider().getNamaBank();
		
		TransferAmount transferamount1 = new TransferAmount();
		transferamount1.setTanggalKirim(Date.valueOf(LocalDate.now()));
		transferamount1.setJumlahAmount(transferamount.getJumlahAmount());
		
		if(asalBank.equals(tujuanBank)) {
			rekeningpengirim.setSaldo(rekeningPengirim.getSaldo()-(transferamount1.getJumlahAmount()));
			transferamount1.setRekpengirim(rekeningpengirim);
			transferamount1.setRekpenerima(rekeningpenerima);
			rekeningPenerima.setSaldo(rekeningPenerima.getSaldo()+(transferamount1.getJumlahAmount()));
			this.tambahRekeningRepository.save(rekeningPengirim);
			this.tambahRekeningRepository.save(rekeningPenerima);
			this.transferAmountRepository.save(transferamount1);
		}else if(asalBank != tujuanBank) {
			System.out.println(transferamount1.getJumlahAmount());
			transferamount1.setFee(6500.0);
			rekeningpengirim.setSaldo(rekeningpengirim.getSaldo()-(transferamount1.getJumlahAmount()-6500.0));
			transferamount1.setRekpengirim(rekeningpengirim);
			transferamount1.setRekpenerima(rekeningpenerima);
			rekeningpenerima.setSaldo(rekeningpenerima.getSaldo()+(transferamount1.getJumlahAmount()));
			this.tambahRekeningRepository.save(rekeningpengirim);
			this.tambahRekeningRepository.save(rekeningpenerima);
			this.transferAmountRepository.save(transferamount1);
		}
	}


	@Override
	public Optional<TransferAmount> getTfbyId(Long Id) {
		// TODO Auto-generated method stub
		return this.transferAmountRepository.findById(Id);
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		this.transferAmountRepository.deleteById(Id);
		
	}

	
}
