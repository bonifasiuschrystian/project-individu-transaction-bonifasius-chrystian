package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;

@Service
public interface TambahRekeningService {
	public List<TambahRekening> getAll();
	public void save(TambahRekening tambahRekening);
	public void getById(Long id);
	public Optional<TambahRekening> getRekeningById(Long id);
	public TambahRekening findByrekening(String rekening);

}
