package com.maybank.assignment.transferdanaBoni.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.assignment.transferdanaBoni.entity.TambahRekening;
import com.maybank.assignment.transferdanaBoni.entity.User;
import com.maybank.assignment.transferdanaBoni.repository.TambahRekeningRepository;

@Service
public class TambahRekeningServiceImplementation implements TambahRekeningService{
	
	@Autowired
	private TambahRekeningRepository tambahRekeningRepository;
	
	
	@Override
	public List<TambahRekening> getAll() {
		// TODO Auto-generated method stub
		return this.tambahRekeningRepository.findAll();
	}

	@Override
	public void save(TambahRekening tambahRekening) {
		// TODO Auto-generated method stub
		TambahRekening tambahRekening2 = new TambahRekening();
		tambahRekening2.setRekening(tambahRekening.getRekening());
		tambahRekening2.setNasabah(tambahRekening.getNasabah());
		tambahRekening2.setBankProvider(tambahRekening.getBankProvider());
		this.tambahRekeningRepository.save(tambahRekening2);
	
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.tambahRekeningRepository.findById(id);
		
	}


	@Override
	public TambahRekening findByrekening(String rekening) {
		// TODO Auto-generated method stub
		return this.tambahRekeningRepository.findByRekening(rekening);
	}

	@Override
	public Optional<TambahRekening> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.tambahRekeningRepository.findById(id);
	}


}
