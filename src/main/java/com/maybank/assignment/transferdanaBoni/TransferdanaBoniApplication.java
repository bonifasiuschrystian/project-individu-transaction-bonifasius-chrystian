package com.maybank.assignment.transferdanaBoni;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maybank.assignment.transferdanaBoni.repository.NasabahRepository;


@SpringBootApplication
public class TransferdanaBoniApplication implements ApplicationRunner{
	@Autowired
	private NasabahRepository nasabahRepository;

	public static void main(String[] args) {
		SpringApplication.run(TransferdanaBoniApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
